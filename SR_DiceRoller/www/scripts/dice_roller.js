﻿var RuleOfSix = false
var success = 0;
var roll_result;

function roll_dice(number) {
    let dice_number;
    if (!number) {
        dice_number = document.getElementById("input_dice").value / 1;
    } else {
        dice_number = number;
    }
    let rand_value_arr = Array.from({ length: dice_number }, () => Math.floor(Math.random() * 6) + 1);
    RuleOfSixNum = rand_value_arr.filter(value => value == 6).length;
    if (RuleOfSix && RuleOfSixNum > 0) {
        roll_dice(RuleOfSixNum).forEach(value => rand_value_arr.push(value));
    }
    return rand_value_arr;
}

function output_result(number)
{
    if (!number) {
        success = 0;
        roll_result = undefined;
    }
    if (roll_result)
    {
        roll_dice(number).forEach(value => roll_result.push(value));
    } else {
        roll_result = roll_dice(number);
    }
    success = roll_result.filter(value => value == 6 || value == 5).length;
    document.getElementById("result").value = roll_result;
    document.getElementById("success").value = "You have: " + success + " success";
}

function getSuccessValue() {
    let re = /\d+/
    return re.exec(document.getElementById("success").value)
}

function reroll_dice() {
    //debugger;
    let reroll_dice_number = document.getElementById("input_dice").value / 1 - getSuccessValue();
    roll_result = Array.from(roll_result.filter(value => value == 6 || value == 5));
    output_result(reroll_dice_number);
}

function increase()
{
    document.getElementById("input_dice").value = document.getElementById("input_dice").value/1 + 1;
}

function decrease() {
    if (document.getElementById("input_dice").value / 1 > 1)
    {
        document.getElementById("input_dice").value = document.getElementById("input_dice").value / 1 - 1;
    }
}

function validate_input()
{;
    let re = /^0/
    document.getElementById("input_dice").value = document.getElementById("input_dice").value.replace(re, "");
}

function ruleOfSix()
{
    if (RuleOfSix)
    {
        RuleOfSix = false;
        document.getElementById("r6_on").style = "display:none;";
        document.getElementById("r6_off").style = "display:normal;";
    } else {
        RuleOfSix = true;
        document.getElementById("r6_on").style = "display:normal;";
        document.getElementById("r6_off").style = "display:none;";
    }
}